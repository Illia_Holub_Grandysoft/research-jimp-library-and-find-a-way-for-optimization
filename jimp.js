// Jimp 68kb to 22 with quality 75%

import Jimp from 'jimp';

async function compressImage() {
    const image = await Jimp.read('./img/fromOtterfish.jpg');
    await image.resize(400, Jimp.AUTO).quality(75).getBufferAsync(image.getMIME());
    await image.writeAsync(`resize_${Date.now()}_400x400.jpg`);
}

compressImage();
