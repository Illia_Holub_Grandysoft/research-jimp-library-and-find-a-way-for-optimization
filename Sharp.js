// Sharp 68kb to 17 with bad quality

import sharp from 'sharp';

async function compressImage() {
    try {
        await sharp("./img/fromOtterfish.jpg")
            .resize({
                width: 400,
                height: 400
            })
            .toFormat("jpeg", { mozjpeg: true })
            .toFile("fromOtterfishCompresed.jpeg");
    } catch (error) {
        console.log(error);
    }
}

compressImage();
