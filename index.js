// imagemin + jimp: 68kb to 21kb with quality 90%

import imagemin from 'imagemin';
import imageminJpegRecompress from 'imagemin-jpeg-recompress';
import Jimp from 'jimp';

const imagePath = './img/fromOtterfish.jpg';

const image = await Jimp.read(imagePath);
const imageResize = await image.resize(400, Jimp.AUTO).quality(100).getBufferAsync(image.getMIME());
await image.writeAsync(imagePath);

const compressImage = await imagemin([imagePath], {
    destination: './img',
    plugins: [
        imageminJpegRecompress({
            accurate: true,
            quality: 'high',
            method: 'smallfry',
            target: 1,
            min: 90,
            loops: 6,
            subsample: true
        }),
    ]
});

console.log(compressImage)

